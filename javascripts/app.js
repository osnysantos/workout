(function($){
  function toLocale() {
    return (new Date()).toLocaleDateString();
  }
  $('#date').val(toLocale());

  function increase_date(t1, t2) {
    var m = (t1.substring(0,t1.indexOf(':'))-0) * 60 +
            (t1.substring(t1.indexOf(':')+1,t1.length)-0) +
            (t2.substring(0,t2.indexOf(':'))-0) * 60 +
            (t2.substring(t2.indexOf(':')+1,t2.length)-0);
    var h = Math.floor(m / 60);
    var total = h + ':' + (m - (h * 60));
    var total_ = h + ' hours and ' + (m - (h * 60))+ ' minutes';
    
    return [total, total_];
  }

  function diminui_data(t1, t2) {
    var m = (t1.substring(0,t1.indexOf(':'))-0) * 60 -
            (t1.substring(t1.indexOf(':')+1,t1.length)-0) -
            (t2.substring(0,t2.indexOf(':'))-0) * 60 -
            (t2.substring(t2.indexOf(':')+1,t2.length)-0);
    var h = Math.floor(m / 60);
    var total2 = h + ':' + (m - (h * 60));
    var total2_ = h + ' hours and ' + (m - (h * 60))+ ' minutes';

    return [total2, total2_];
  }




  var Item = Backbone.Model.extend({
    
  });

  var List = Backbone.Collection.extend({
    
    model: Item
  });

  var ItemView = Backbone.View.extend({
    defaults: {
      total: 10
    },
    tagName: 'tr',
    events: {
      'click .destroy': 'destroy'
    },
    initialize: function(){
      _.bindAll(this, 'render', 'unrender', 'destroy', 'decrease_time');
      this.listenTo(this.model, 'destroy', this.unrender);
    },
    render: function(){
      $(this.el).html('<td>'+this.model.get('time')+'</td><td>'+this.model.get('name')+'</td><td><a href="javascript:void(0);" class="destroy">x</a>'+this.model.get('date')+'</td>');
      $('.time strong').html(this.model.get('total'));
      return this;
    },
    
    destroy: function(){
      this.model.destroy();
    },
    
    unrender: function(){
      this.decrease_time(this.total, this.model.get('time'));
      $(this.el).fadeOut('400', function() {
        $(this).remove();
      });
    },

    decrease_time: function(time1, time2){
      console.log('----');
      console.log(time1);
      console.log(time2);

      time = diminui_data(time1, time2);
      this.total = time[0];

      console.log(this.total);
      // console.log(time[0]);
      // console.log(this.total);
      $('.total strong').html(time[1]);
    },

    
  });

  var ListView = Backbone.View.extend({

    el: $('body'),
    events: {
      'submit form': 'addItem',
      'click .destroy': 'destroy'
    },
    initialize: function(){
      _.bindAll(this, 'render', 'addItem', 'appendItem', 'increase_time');

      this.collection = new List();
      this.collection.bind('add', this.appendItem);

      this.total = '00:00';
      this.render();
    },
    render: function(){
      var self = this;
      _(this.collection.models).each(function(item){
        self.appendItem(item);
      }, this);
    },

    
    
    addItem: function(e){
      e.preventDefault();

      
      
      var item = new Item();
      item.set({
        time: $('form .hour').find(":selected").val()+':'+$('form .minutes').find(":selected").val(),
        time_ext: $('form .hour').find(":selected").val()+'h'+$('form .minutes').find(":selected").val()+'m',
        name: $('form .type').find(":selected").text(),
        date: $('form #date').val(),
        // total: this.total
      });
      this.collection.add(item);
      $('form').find("input[type=text], textarea").val("");

      $('#date').val(toLocale());


      t1 = $('form .hour').find(":selected").val()+':'+$('form .minutes').find(":selected").val();
      this.increase_time(this.total, t1);

      this.total = time[0];
      $('.center').hide();
      console.log(this.total);
      
    },

    increase_time: function(time1, time2){
      time = increase_date(time1, time2);
      this.total = time[0];
      $('.total strong').html(time[1]);
    },
    
    appendItem: function(item){
      var itemView = new ItemView({
        model: item
      });
      $('tbody', this.el).prepend(itemView.render().$el.addClass('hide').fadeIn());
    },
    
  });

  var listView = new ListView();
})(jQuery);
